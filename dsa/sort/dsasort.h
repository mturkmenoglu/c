#ifndef DSASORT_H
#define DSASORT_H

void bubbleSort(int *array, int n);
void insertionSort(int *array, int n);
void selectionSort(int *array, int n);
void shellSort(int *array,int n);

#endif